from django.db import models

# Create your models here.
from django.db import models


class Subscribers(models.Model):
    """A subscriber model."""

    email = models.EmailField(max_length=100, blank=False, null=False, help_text='', unique=True, error_messages={'invalid':"Enter a valid email address"})


    def __str__(self):
        """Str repr of this object."""
        return self.email

    class Meta:  # noqa
        verbose_name = "Susbcriber"
        verbose_name_plural = "Subscribers"
