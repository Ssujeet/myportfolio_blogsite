from django.apps import AppConfig


class TutorailsConfig(AppConfig):
    name = 'tutorails'
