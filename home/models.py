from django.db import models

from wagtail.core.models import Page
from blogs.models import Blog
from projects.models import Project
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel
)
from wagtail.images.edit_handlers import ImageChooserPanel

import subscribers.helper


class HomePage(Page):
    book_file = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    max_count = 1
    Description = models.TextField(blank=True)
    thumbNail = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    content_panels = Page.content_panels + [
        DocumentChooserPanel('book_file'),
        FieldPanel("Description", classname="full"),
        ImageChooserPanel("thumbNail"),
    ]

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)

        all_posts = Blog.objects.live().public().order_by(
            '-first_published_at')[0:7]
        all_projects = Project.objects.live().public().order_by(
            '-first_published_at')[0:7]

        context["posts"] = all_posts
        context["all_projects"] = all_projects
        return context
