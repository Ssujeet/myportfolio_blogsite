from django.db import models
from django.db.models import F, Q, When
from django.shortcuts import render, redirect
from django.utils import timezone
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable, Page
from django import forms
from wagtail.snippets.models import register_snippet
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from taggit.models import TaggedItemBase, Tag as TaggitTag
import datetime
from datetime import date, timedelta
from django.conf import settings
from django.http import HttpResponseRedirect
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel,
    PageChooserPanel,
)
from modelcluster.tags import ClusterTaggableManager
from wagtail.images.edit_handlers import ImageChooserPanel
from autoslug import AutoSlugField
from wagtailmd.utils import MarkdownField, MarkdownPanel
from subscribers.models import Subscribers
from modelcluster.fields import (
    ParentalKey,
    ParentalManyToManyField
)


class BlogIndexPage(RoutablePageMixin, Page):
    intro = RichTextField(blank=True)
    pagination_amount = models.PositiveSmallIntegerField(default=5)
    content_panels = Page.content_panels + [
        FieldPanel("intro"),
        FieldPanel("pagination_amount")
    ]

    subpage_types = [
        "blogs.Blog",
    ]
    max_count = 1

    @property
    def get_child_pages(self):
        return self.get_children().public().live()
        # return self.get_children().public().live().values("id", "title", "slug")

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)

        context_data = self.getContextData(request)
        context.update(context_data)
        return context

    @route(r'^subscribe/$')
    def the_subscribe_page(self, request, *args, **kwargs):
        context = self.get_context(request, *args, **kwargs)
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            mail = request.POST.get("email")
            try:
                subscribe = Subscribers(email=mail)
                subscribe.save()
            except:
                print("Error Occurred")

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def get_posts(self):
        return BlogIndexPage.objects.descendant_of(self).live()

    def getContextData(self, request):
        context = {}
        all_posts = Blog.objects.live().public().order_by(
            '-first_published_at')
        # latest_posts = Blog.objects.live().public().order_by(
        #     '-first_published_at')[0]
        # Paginate all posts by 2 per page
        paginator = Paginator(all_posts, self.pagination_amount)
        # Try to get the ?page=x value
        page = request.GET.get("page")
        try:
            # If the page exists and the ?page=x is an int
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If the ?page=x is not an int; show the first page
            posts = paginator.page(1)
        except EmptyPage:
            # If the ?page=x is out of range (too high most likely)
            # Then return the last page
            posts = paginator.page(paginator.num_pages)

        # "posts" will have child pages; you'll need to use .specific in the template
        # in order to access child properties, such as youtube_video_id and subtitle
        if(len(all_posts) > 1):
            context["posts"] = posts[1:]
        context["page"] = self
        context["latest_posts"] = posts[0]
        context["categories"] = BlogCategory.objects.all()
        return context


class Blog(Page):
    created_date = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    Short_Description = models.TextField(blank=True)
    Content = MarkdownField()
    Categories = ParentalManyToManyField("blogs.BlogCategory", blank=True)
    tags = ClusterTaggableManager(through="blogs.BlogTag", blank=True)
    thumbNail = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    subpage_types = []
    minRead = models.PositiveSmallIntegerField(default=5)
    content_panels = Page.content_panels + [
        FieldPanel("Short_Description", classname="full"),
        FieldPanel('tags'),
        MarkdownPanel("Content"),
        FieldPanel('Categories', widget=forms.CheckboxSelectMultiple),
        ImageChooserPanel("thumbNail"),
        FieldPanel('minRead'),
    ]
    parent_page_types = ['blogs.BlogIndexPage']

    @property
    def get_Blog_index_page(self):
        return self.get_parent().specific

    @property
    def isBlog_updated(self):
        print(self.created_date.date() > self.updated.date())
        return self.created_date.strftime('%Y-%m-%d %H:%M:%S') < self.updated.strftime('%Y-%m-%d %H:%M:%S')

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        posts = Blog.objects.live().public().order_by(
            '-first_published_at')
        context["posts"] = posts[:3]
        return context


@register_snippet
class BlogCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='name')
    panels = [
        FieldPanel('name'),

    ]

    def __str__(self):
        return self.name

    def get_all_category(self):
        all_category = BlogCategory.Objects.all()

        return all_category

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ["name"]


class BlogTag(TaggedItemBase):
    content_object = ParentalKey('Blog', related_name='Blog_tag')


@register_snippet
class Tag(TaggitTag):
    class Meta:
        proxy = True
