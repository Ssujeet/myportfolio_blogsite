# Generated by Django 3.1.4 on 2020-12-11 14:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='GitLabLink',
            field=models.URLField(blank=True, verbose_name='GitLabLin URL'),
        ),
        migrations.AddField(
            model_name='project',
            name='IsCodeProtected',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='project',
            name='LiveDemoLink',
            field=models.URLField(blank=True, verbose_name='LiveDemoLink URL'),
        ),
    ]
