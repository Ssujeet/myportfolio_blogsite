from django.db import models
from django.db.models import F, Q, When
from django.shortcuts import render, redirect
from django.utils import timezone
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable, Page
from django import forms
from wagtail.snippets.models import register_snippet
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from taggit.models import TaggedItemBase, Tag as TaggitTag
import datetime
from datetime import date, timedelta
from django.conf import settings
from django.http import HttpResponseRedirect
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel,
    PageChooserPanel,
)
from modelcluster.tags import ClusterTaggableManager
from wagtail.images.edit_handlers import ImageChooserPanel
from autoslug import AutoSlugField
from wagtailmd.utils import MarkdownField, MarkdownPanel
from modelcluster.fields import (
    ParentalKey,
    ParentalManyToManyField
)


class ProjectIndexPage(RoutablePageMixin, Page):
    intro = RichTextField(blank=True)
    pagination_amount = models.PositiveSmallIntegerField(default=5)
    content_panels = Page.content_panels + [
        FieldPanel("intro"),
        FieldPanel("pagination_amount")
    ]

    subpage_types = [
        "projects.Project",
    ]
    max_count = 1

    @property
    def get_child_pages(self):
        return self.get_children().public().live()
        # return self.get_children().public().live().values("id", "title", "slug")

    def get_context(self, request, *args, **kwargs):
        """Adding custom stuff to our context."""
        context = super().get_context(request, *args, **kwargs)

        all_posts = Project.objects.live().public().order_by(
            '-first_published_at')
        # latest_posts = Project.objects.live().public().order_by(
        #     '-first_published_at')[0]
        # Paginate all posts by 2 per page
        paginator = Paginator(all_posts, self.pagination_amount)
        # Try to get the ?page=x value
        page = request.GET.get("page")
        try:
            # If the page exists and the ?page=x is an int
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If the ?page=x is not an int; show the first page
            posts = paginator.page(1)
        except EmptyPage:
            # If the ?page=x is out of range (too high most likely)
            # Then return the last page
            posts = paginator.page(paginator.num_pages)

        # "posts" will have child pages; you'll need to use .specific in the template
        # in order to access child properties, such as youtube_video_id and subtitle
        context["all_projects"] = posts
        context["page"] = self
        # context["latest_posts"] = latest_posts
        context["categories"] = ProjectCategory.objects.all()
        return context

    def get_posts(self):
        return ProjectIndexPage.objects.descendant_of(self).live()


class Project(Page):
    created_date = models.DateTimeField(auto_now_add=True)
    Short_Description = models.TextField(blank=True)
    GitLabLink = models.URLField("GitLabLin URL", blank=True)
    LiveDemoLink = models.URLField("LiveDemoLink URL", blank=True)
    Categories = ParentalManyToManyField(
        "projects.ProjectCategory", blank=True)
    IsCodeProtected = models.BooleanField(default=False)
    IsLiveDemo = models.BooleanField(default=True)
    thumbNail = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.SET_NULL, null=True, related_name="+"
    )
    subpage_types = []
    content_panels = Page.content_panels + [
        FieldPanel("Short_Description", classname="full"),
        FieldPanel('GitLabLink'),
        FieldPanel('LiveDemoLink'),
        FieldPanel('IsCodeProtected'),
        FieldPanel('IsLiveDemo'),
        FieldPanel('Categories', widget=forms.CheckboxSelectMultiple),
        ImageChooserPanel("thumbNail"),
    ]
    parent_page_types = ['projects.ProjectIndexPage']

    @property
    def get_Project_index_page(self):
        return self.get_parent().specific

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        return context


@register_snippet
class ProjectCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='name')
    panels = [
        FieldPanel('name'),

    ]

    def __str__(self):
        return self.name

    def get_all_category(self):
        all_category = ProjectCategory.Objects.all()
        return all_category

    class Meta:
        verbose_name = 'ProjectCategory'
        verbose_name_plural = 'ProjectCategories'
        ordering = ["name"]
