from wagtail.core.signals import page_published
from blogs.models import Blog
import threading
from django.core.mail import send_mail
from .models import Subscribers
from django.conf import settings
import smtplib
import ssl
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def receiver(sender, **kwargs):
    subscribers_list = []
    latest_blog = Blog.objects.live().public().order_by('-first_published_at').first()
    all_subscribers = Subscribers.objects.all()
    link = "http://127.0.0.1:8000{}".format(latest_blog.url)
    latest_editBlog = Blog.objects.live().public().order_by(
        '-last_published_at').first()
    print(latest_editBlog.Short_Description, latest_blog.Short_Description)
    if not latest_editBlog.pk == latest_blog.pk:
        print(latest_editBlog.pk, latest_blog.pk)
        return
    if latest_blog.created_date.strftime('%Y-%m-%d %H:%M:%S') < latest_blog.updated.strftime('%Y-%m-%d %H:%M:%S'):
        print("yes i did it")
        return

    image_link = "http://127.0.0.1:8000/media/images/{}".format(
        latest_blog.thumbNail)
    print(latest_blog.thumbNail)
    sender_email = "rmorty54nepal@gmail.com"
    password = "ojqqptwermxlslkc"
    message = MIMEMultipart("alternative")
    message["Subject"] = latest_blog.title
    message["From"] = sender_email
    context = ssl.create_default_context()

    try:
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            for subscriber in all_subscribers:
                unsubscribelink = "http://127.0.0.1:8000/blogs/unsubscribe/{}".format(
                    subscriber.pk)
                text = """\
              Hi,
              How are you?
              Read our Latest Blog
              """
                html = """\
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml">
          
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>A Simple Responsive HTML Email</title>

          </head>

          <body yahoo bgcolor="#e5e5e5" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%!important;" >
          <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <!--[if (gte mso 9)|(IE)]>
                <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td>
              <![endif]-->     
              <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="width:100%;max-width:600px;" >
                <tr>            
                    
                                  <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%;max-width:425px;" >  
                      <tr>
                        <td height="70">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          
                            
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                          </td>
                        </tr>
                    </table>
                    <![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class="innerpadding borderbottom" style="padding-top:30px;padding-bottom:30px;padding-right:30px;padding-left:30px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#f2eeed;" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="h2" style="color:#153643;font-family:sans-serif;padding-top:0;padding-bottom:15px;padding-right:0;padding-left:0;font-size:24px;line-height:28px;font-weight:bold;" >
                        {}
                        </td>
                      </tr>
                    
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="innerpadding borderbottom" style="padding-top:30px;padding-bottom:30px;padding-right:30px;padding-left:30px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#f2eeed;" >
                  
                    <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%;max-width:380px;" >  
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="bodycopy" style="color:#153643;font-family:sans-serif;font-size:16px;line-height:22px;" >
                              {}
                              </td>
                            </tr>
                            <tr>
                              <td style="padding-top:20px;padding-bottom:0;padding-right:0;padding-left:0;" >
                                <table class="buttonwrapper" bgcolor="#e05443" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="button" height="45" style="text-align:center;font-size:18px;font-family:sans-serif;font-weight:bold;padding-top:0;padding-bottom:0;padding-right:30px;padding-left:30px;" >
                                      <a href={} style="color:#ffffff;text-decoration:none;" >READ MORE</a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                          </td>
                        </tr>
                    </table>
                    <![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class="innerpadding borderbottom" style="padding-top:30px;padding-bottom:30px;padding-right:30px;padding-left:30px;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#f2eeed;" >
                    <img class="fix" src={} width="100%" border="0" alt="" style="height:auto;" />
                  </td>
                </tr>
                <tr>
                  <td class="innerpadding bodycopy" style="padding-top:30px;padding-bottom:30px;padding-right:30px;padding-left:30px;color:#153643;font-family:sans-serif;font-size:16px;line-height:22px;" >
                      You are seeing this mail because you are subscribed to our mailing list.
                  </td>
                </tr>
                <tr>
                  <td class="footer" bgcolor="#44525f" style="padding-top:20px;padding-bottom:15px;padding-right:30px;padding-left:30px;" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center" style="padding-top:20px;padding-bottom:0;padding-right:0;padding-left:0;" >
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <!--[if (gte mso 9)|(IE)]>
                    </td>
                  </tr>
              </table>
              <![endif]-->
              </td>
            </tr>
          </table>
          </body>
          </html>
              """.format(latest_blog.title, latest_blog.Short_Description, link,  image_link,)

                # Turn these into plain/html MIMEText objects
                part1 = MIMEText(text, "plain")
                part2 = MIMEText(html, "html")

                message.attach(part1)
                message.attach(part2)
                server.sendmail(
                    sender_email, subscriber.email, message.as_string()
                )
    except:
        print("Error Occured")


def task(sender, **kwargs):
    # receiver(sender)
    thread = threading.Thread(target=receiver, args=(sender,))
    thread.daemon = True
    thread.start()


page_published.connect(task, sender=Blog)
